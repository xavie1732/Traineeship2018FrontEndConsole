
package mainApp;


import ApplicationParts.*;
import colruyt.pcrsejb.bo.user.UserBo;

public class AppStarter {
	public static  UserBo currentUser;
	private static Application app;
	private static ApplicationPart part;


	public static void main(String[] args) {
		app = new Application();
		//app.fillDB();
		boolean run = true;
		while (run) {
			currentUser = app.login();
			boolean loggedIn = true;
			while (loggedIn) {
				String choice = app.mainMenu(currentUser);
				part = null;
				switch (choice) {
				case "Adapt Profile":
					part = new AdaptProfilePart();
					break;
				case "Fill Survey":
					part = new SurveyApplication(currentUser);
					break;
				case "View History":
					part = new ViewHistoryApplication();
					break;
				case "Start Consensus":
					part = new ConsensusApplication();
					break;
				case "Craft Competence Management for function":
					//TODO het management van Craft competences door een functieverantwoordelijke
					break;
				case "Behavioral Competence Management for function":
					//TODO het management van behavioral competence door een functieverantwoordelijke
					break;
				case "Manage Roles for function":
					//TODO het management van rollen binnen een functie
					break;
				case "Fill Survey for team member":
					part = new TeamSurveyApplication();
					break;
				case "Team Management":
					part = new TeamManagementApplication();
					break;
				case "Craft Competence Management for operating unit":
					//TODO het management van craft competences binnen het operating unit
					break;
				case "Craft Competence Management for application":
					part = new CraftCompetenceManagmentApplication();
					break;
				case "Role Management for application":
					part = new RoleManagementApplication();
					break;
				case "user Management for application":
					part = new UserManagementApplication();
					break;
				case "Add Team":
					part = new AddTeamApplication();
					break;
				case "Function Responsible Management":
					//TODO Management van Functieverantwoordelijken
					part = new FunctionResponsibleApplication();
					break;
				case "Log Off":
					loggedIn = false;
					break;
				case "Quit Application":
					loggedIn = false;
					run = false;
					break;
				default:
					break;
				}
				if (run & loggedIn) {
					part.run();
				}
			}
		}
		System.out.println("APPLICATION SHUT DOWN");
	}
}
