package mainApp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import ApplicationHelpers.Prompter;
import colruyt.pcrsejb.bo.privileges.AdminPrivilegeBo;
import colruyt.pcrsejb.bo.privileges.DirectorPrivilegeBo;
import colruyt.pcrsejb.bo.privileges.FunctionResponsiblePrivilegeBo;
import colruyt.pcrsejb.bo.privileges.PrivilegeBo;
import colruyt.pcrsejb.bo.privileges.TeamManagerPrivilegeBo;
import colruyt.pcrsejb.bo.privileges.TeamMemberPrivilegeBo;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.bo.userPrivilege.UserPrivilegeBo;
import colruyt.pcrsejb.facade.UserFacade;


public class Application {
	private UserBo superUser;
	private UserFacade userFacade = new UserFacade();
	
//	public void fillDB() {
//		HashSet<PrivilegeBo> privileges1 = new HashSet<>();
//		privileges1.add(new TeamMemberPrivilegeBo());
//		UserBo user1 = new UserBo( "Laura", "Lynn", "Laura_lynn@gmail.com", "laura", privileges1, "BE");
//		userFacade.addUser(user1);
//
//		HashSet<PrivilegeBo> privileges2 = new HashSet<>();
//		privileges2.add(new TeamManagerPrivilegeBo());
//		UserBo user2 = new UserBo( "Wouter", "Mosselmans", "WouterMosselmans@gmail.com", "wouter", privileges2);
//		userFacade.addUser(user2);
//
//		HashSet<PrivilegeBo> privileges3 = new HashSet<>();
//		privileges3.add(new FunctionResponsiblePrivilegeBo());
//		UserBo user3 = new UserBo( "Maarten", "Spooren", "MaartenSpooren@gmail.com", "maarten", privileges3);
//		userFacade.addUser(user3);
//
//		HashSet<PrivilegeBo> privileges4 = new HashSet<>();
//		privileges4.add(new DirectorPrivilegeBo());
//		UserBo user4 = new UserBo( "Director", "Joe", "DirectorJoe@gmail.com", "director", privileges4);
//		userFacade.addUser(user4);
//
//		HashSet<PrivilegeBo> privileges5 = new HashSet<>();
//		privileges5.add(new AdminPrivilegeBo());
//		UserBo user5 = new UserBo( "Admin", "Is Trator", "AdminIsTrator@gmail.com", "admin", privileges5);
//		userFacade.addUser(user5);
//
//		userFacade.addUser(superUser);
//	}
	
	public UserBo login() {
		boolean loggedIn = false;
		UserBo currentUser = null;
		while (!loggedIn) {
			currentUser = Prompter.promptMenu("Hello, Who are you?", userFacade.getAllUsers());
			loggedIn = true;
			//TODO PASSWORD CHECKING
		}
		return currentUser;
	}
	
	public String mainMenu(UserBo currentUser) {
		List<String> selectableOptions = new ArrayList<>();
		selectableOptions.add("Adapt Profile");
		for (UserPrivilegeBo pr : currentUser.getPrivilegeBoHashSet()) {
			String privDescr = pr.getClass().getSimpleName();
			switch (privDescr) {
			case "TeamMemberPrivilegeBo":
				selectableOptions.add("Fill Survey");
				selectableOptions.add("View History");
				selectableOptions.add("Start Consensus");
				break;
			case "FunctionResponsiblePrivilegeBo":
				selectableOptions.add("Craft Competence Management for function");
				selectableOptions.add("Behavioral Competence Management for function");
				selectableOptions.add("Manage Roles for function");
				break;
			case "TeamManagerPrivilegeBo":
				selectableOptions.add("Fill Survey for team member");
				selectableOptions.add("Team Management");
				break;
			case "DirectorPrivilegeBo":
				selectableOptions.add("Craft Competence Management for company");
				break;
			case "AdminPrivilegeBo":
				selectableOptions.add("Craft Competence Management for application");
				selectableOptions.add("Role Management for application");
				selectableOptions.add("user Management for application");
				selectableOptions.add("Add Team");
				selectableOptions.add("Function Responsible Management");
				break;
			default:
				break;
			}
		}
		selectableOptions.add("Log Off");
		selectableOptions.add("Quit Application");
		return Prompter.promptMenu("Main menu", selectableOptions);
	}
	
	
}
