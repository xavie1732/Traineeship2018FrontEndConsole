package ApplicationHelpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

public abstract class Prompter {
	
	public static <T> T promptMenu(String question, List<T> options) {
		System.out.println(question);
		Integer i = 1;
		for (T option : options) {
			System.out.println(i + ". " + option.toString());
			i = i + 1;
		}
		return options.get(readNumber("Please insert your number: ", 1,i - 1) - 1);
	}
	
	public static int promptRating(String question, List<String> options) {
		System.out.println(question);
		Integer i = 1;
		for (String option : options) {
			System.out.println(i + ". " + option);
			i = i + 1;
		}
		return readNumber("Please insert your rating: ", 1,i - 1);
	}
	
	public static String promptString(String question) {
		boolean validInput = false;
		String input = null;
		while (!validInput) {
			try {
				System.out.println(question);
				InputStreamReader streamReader = new InputStreamReader(System.in);
			    BufferedReader bufferedReader = new BufferedReader(streamReader);
			    input = bufferedReader.readLine();
			    //at least one character
			    if (input.isEmpty()) 
			    {
			    	throw new Exception();
			    }
			    validInput = true;
			} catch (Exception ex) {
				System.out.println("Enter at least one character!");
			}	
		}
		return input;
	}
	
	public static String promptEmailAddress(String question)
	{
		boolean validInput = false;
		String input = null;
		while(!validInput) {
			try {
				input = promptString(question);
				if(!input.matches("^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}$"))
				{
					throw new Exception();
				}
				validInput = true;
			} catch (Exception ex) {
				System.out.println("This is not a valid e-mail address");
			}
		}
		return input;
	}
	
	public static String promptAlphabeticString(String question) {
		boolean validInput = false;
		String input = null;
		while (!validInput) {
			try {
			    input = promptString(question);
			    //ONLY Alphabetic characters
			    if (!input.matches("[a-zA-Z]+")) {
			    	throw new Exception();
			    }
			    validInput = true;
			} catch (Exception ex) {
				System.out.println("That is not a alphabetical string!");
			}
		}
		return input;
	}
	
	public static String promptAlphabeticStringOfLength(String question, Integer length) {
		boolean validInput = false;
		String input = null;
		while (!validInput) {
			try {
				input = promptAlphabeticString(question);
				//EXACT length characters
			    if (input.length() != length) 
			    {
			    	throw new Exception();
			    }
			    validInput = true;
			} catch (Exception ex) {
				System.out.println("That string is too short!");
			}
		}
		return input;
	}
	
	public static boolean yesOrNoQuestion(String question) {
		boolean validInput = false;
		String input;
		boolean yesOrNo = false;
		while (!validInput) {
			try {
			    input = promptAlphabeticStringOfLength(question + " (Y/N)", 1);
			    //Only 'Y' or 'N'
			    if (!(input.equalsIgnoreCase("Y") | input.equalsIgnoreCase("N"))) {
			    	throw new Exception();
			    }
			    if (input.equalsIgnoreCase("Y")) {
			    	yesOrNo = true;
			    }
			    validInput = true;
			} catch (Exception ex) {
				System.out.println("That is not a valid option!");
			}
		}
		return yesOrNo;
	}
	
	private static Integer readNumber(String question, Integer min, Integer max) {
		boolean validInput = false;
		String input;
		Integer intReturn = 0;
		while (!validInput) {
			try {
				input = promptString(question);
				intReturn = Integer.valueOf(input);
				if (intReturn >= min && intReturn <= max) {
					validInput = true;
				}
				else {
					throw new Exception();
				}
			} catch (IOException ex) {
			    ex.printStackTrace();
			} catch (Exception ex) {
				System.out.println("That's not a valid number.");
			}
		}
		return intReturn;
	}
}
