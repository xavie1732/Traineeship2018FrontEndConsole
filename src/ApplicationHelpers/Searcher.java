package ApplicationHelpers;

import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.facade.UserFacade;

import java.util.List;

public abstract class Searcher {
	
	public static UserBo askUserUsingShortString() {
		boolean validInput = false;
		String input;
		List<UserBo> matchingUsers = null;
		while (!validInput) {
			try {
				input = Prompter.promptAlphabeticStringOfLength("Please insert the user search string:",5);
				matchingUsers = searchUsers(input);
			    if (matchingUsers.isEmpty()) {
			    	throw new Exception();
			    }
			    validInput = true;
			} catch (Exception ex) {
				System.out.println("That is not a valid string or the person does not exist!");
			}
		}
		return Prompter.promptMenu("Which of these users do you want to select?", matchingUsers);
	}
	
	private static List<UserBo> searchUsers(String shortString) {
		UserFacade userFacade = new UserFacade();
//		HashSet<PrivilegeBo> privileges1 = new HashSet<>();
//		privileges1.add(new TeamMemberPrivilegeBo());
//		UserBo user1 = new UserBo( "Laura", "Lynn", "Laura_lynn@gmail.com", "laura", privileges1, "BE");
//		userFacade.addUser(user1);
//
//		HashSet<PrivilegeBo> privileges2 = new HashSet<>();
//		privileges2.add(new TeamManagerPrivilegeBo());
//		UserBo user2 = new UserBo( "Wouter", "Mosselmans", "WouterMosselmans@gmail.com", "wouter", privileges2, "BE");
//		userFacade.addUser(user2);
//
//		HashSet<PrivilegeBo> privileges3 = new HashSet<>();
//		privileges3.add(new FunctionResponsiblePrivilegeBo());
//		UserBo user3 = new UserBo( "Maarten", "Spooren", "MaartenSpooren@gmail.com", "maarten", privileges3, "BE");
//		userFacade.addUser(user3);
//
//		HashSet<PrivilegeBo> privileges4 = new HashSet<>();
//		privileges4.add(new DirectorPrivilegeBo());
//		UserBo user4 = new UserBo( "Director", "Joe", "DirectorJoe@gmail.com", "director", privileges4, "BE");
//		userFacade.addUser(user4);
//
//		HashSet<PrivilegeBo> privileges5 = new HashSet<>();
//		privileges5.add(new AdminPrivilegeBo());
//		UserBo user5 = new UserBo( "Admin", "Is Trator", "AdminIsTrator@gmail.com", "admin", privileges5, "BE");
//		userFacade.addUser(user5);

		return userFacade.searchUsers(shortString);
	}
}
