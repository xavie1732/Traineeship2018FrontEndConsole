package ApplicationParts;

import java.util.ArrayList;
import java.util.List;

import ApplicationHelpers.Prompter;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.facade.TeamFacade;
import colruyt.pcrsejb.facade.UserFacade;
import colruyt.pcrsejb.service.dl.DbService;
import mainApp.AppStarter;

public class AdaptProfilePart extends DbService implements ApplicationPart {

	UserFacade uf = new UserFacade();
	TeamFacade tf = new TeamFacade();
	
	
	@Override
	public void run() {
		boolean back = false;
		List<String> menuOptions = new ArrayList<>();
		menuOptions.add("Change password");
		menuOptions.add("Progress");
		menuOptions.add("Profile");
        while (!back) {
            System.out.println("---------------");
            System.out.println("ADAPT PROFILE");
            System.out.println("---------------");
            String choice = Prompter.promptMenu("Please choose your functionality?", menuOptions);
            switch (choice) {
                case "Change password":
                	changePassword();
                    break;
                case "Progress":
                    progress();
                    break;
                case "Profile":
                    profile();
                    break;
                default:
                    break;
            }
        }
    }

	/**
	 * Methode voor het weergeven van het profiel
	 */
	private void profile() {
//		System.out.println("---------------");
//        System.out.println("View Profile");
//        System.out.println("---------------");
//        System.out.println("Name " + AppStarter.currentUser.getFirstName() + " " + AppStarter.currentUser.getLastName());
//        String function = null;
//        for (PrivilegeBo priv : AppStarter.currentUser.getPrivilegeBoHashSet()) {
//        	if (priv instanceof TeamMemberPrivilege) {
//        		function = ((TeamMemberPrivilege) priv).getFunction().getTitle();
//        		System.out.println("Function " + function);
//        	}
//        }
	}

	/**
	 * Methode die de progress van een bepaalde persoon 
	 * en van de teamManager weergeeft
	 */
	private void progress() {
		System.out.println("---------------");
        System.out.println("View Progress");
        System.out.println("---------------");
        System.out.println(AppStarter.currentUser.getFirstName() + " " + AppStarter.currentUser.getLastName());
        //TODO .getProgress
        UserBo manager = tf.getManager(tf.getTeam(AppStarter.currentUser));
        System.out.println(manager.getFirstName() + " " + manager.getLastName());
        //TODO .getProgress
	}

	/**
	 * Methode voor het veranderen van het paswoord
	 */
	private void changePassword() {
		String currentPassword = Prompter.promptAlphabeticString("Please give the current password.");
		String newPassword = Prompter.promptAlphabeticString("Please give the new password.");
		String confirmPassword = Prompter.promptAlphabeticString("Please confirm the new password.");
		AppStarter.currentUser.setPassword(newPassword);
		uf.saveUser(AppStarter.currentUser);	
	}
	
}
