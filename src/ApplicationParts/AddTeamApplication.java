package ApplicationParts;

import ApplicationHelpers.Prompter;
import ApplicationHelpers.Searcher;
import colruyt.pcrsejb.bo.team.TeamBo;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.facade.TeamFacade;

public class AddTeamApplication implements ApplicationPart {

	TeamFacade tf = new TeamFacade();
	
	
	@Override
	public void run() {
		boolean back = false;
		while(!back)
		{
			System.out.println("------------");
			System.out.println("ADD NEW TEAM");
			System.out.println("------------");
			String teamName = Prompter.promptAlphabeticString("Please give the team name.");
			System.out.println("Please give the name of the team manager");
			UserBo teamManager = Searcher.askUserUsingShortString();
			TeamBo teamBo = new TeamBo(teamName, teamManager);
			tf.addTeam(teamBo);
		}
	}	
}
