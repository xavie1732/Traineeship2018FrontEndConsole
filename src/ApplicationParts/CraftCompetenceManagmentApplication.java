package ApplicationParts;

import ApplicationHelpers.Prompter;
import colruyt.pcrsejb.bo.competence.CraftCompetenceBo;
import colruyt.pcrsejb.facade.CompetenceFacade;

import java.util.ArrayList;
import java.util.List;

public class CraftCompetenceManagmentApplication implements ApplicationPart{
    
    private CompetenceFacade craftCompetencef = new CompetenceFacade();
    @Override
    public void run(){
        boolean back = false;
        List<String> menuOptions = new ArrayList<>();
        menuOptions.add("Add CraftCompetence");
        menuOptions.add("Remove CraftCompetence");
        menuOptions.add("Get ALL CraftCompetence");
        menuOptions.add("Go back");
        while (!back) {
            System.out.println("---------------");
            System.out.println("Craft Competence MANAGEMENT");
            System.out.println("---------------");
            String choice = Prompter.promptMenu("Please choose your functionality?", menuOptions);
            switch (choice) {
                case "Add CraftCompetence":
                    AddCraftCompetence();
                    break;
                case "Remove CraftCompetence":
                    RemoveCraftCompetence();
                    break;
                case "Get All CraftCompetence":
                    // TODO make this work!
                    //craftCompetencef.getAllCraftCompetences().forEach(System.out::println);
                    break;
                case "Go back":
                    back = true;
                default:
                    break;
            }
        }
    }
    
    private void AddCraftCompetence(){
        System.out.println("craft Competence CREATION");
        System.out.println("-------------");
        String name = Prompter.promptString("Please give the name of a craft competence.");
        String discription = Prompter.promptString("Please give discription of a craft competence.");


        CraftCompetenceBo craftCompetence = new CraftCompetenceBo();
        craftCompetence.setName(name);
        craftCompetence.setDescription(discription);

        System.out.println("craft Competence created...");

        //TODO Make this method!
        //craftCompetencef.addCraftCompetence(craftCompetence);
    }

    private void RemoveCraftCompetence(){
        System.out.println("craft Competence DELETION by name");
        System.out.println("-------------");
        String name = Prompter.promptString("Please give the name of a craft competence.");

        //CraftCompetenceBo user = name;
        // TODO
        //craftCompetencef.removeCraftCompetenceByName(name);
        System.out.println("craft Competence deleted...");
    }
}
