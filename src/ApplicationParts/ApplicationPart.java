package ApplicationParts;

public interface ApplicationPart {
	void run();
}
