package ApplicationParts;

import ApplicationHelpers.Prompter;
import colruyt.pcrsejb.bo.competence.BehavioralCompetenceBo;
import colruyt.pcrsejb.bo.competence.CompetenceBo;
import colruyt.pcrsejb.bo.competence.CraftCompetenceBo;
import colruyt.pcrsejb.bo.survey.ConsensusRatingBo;
import colruyt.pcrsejb.bo.survey.RatingBo;
import colruyt.pcrsejb.bo.survey.SurveyBo;
import colruyt.pcrsejb.bo.survey.SurveySetBo;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.entity.survey.SurveyKind;
import colruyt.pcrsejb.facade.RatingFacade;
import colruyt.pcrsejb.facade.SurveyFacade;
import mainApp.AppStarter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;

public class SurveyApplication implements ApplicationPart {
    private SurveyFacade sf = new SurveyFacade();
    private RatingFacade rf = new RatingFacade();

    private SurveyKind surveyKind;
    private boolean editable;

    private SurveyBo surveyBo;

    public SurveyApplication(boolean consensus) {
        this.editable = true;
        if (!consensus) {
            surveyKind = SurveyKind.TeamMember;
        } else {
            surveyKind = SurveyKind.Consensus;
        }
        surveyBo = sf.getLastSurveySetForUser(AppStarter.currentUser).getSurveyBoSet().get(surveyKind);
    }

    public SurveyApplication(UserBo subject) {
        this.editable = true;
        surveyKind = SurveyKind.TeamManager;
        surveyBo = sf.getLastSurveySetForUser(subject).getSurveyBoSet().get(surveyKind);
    }

    public SurveyApplication(SurveyBo surveyBo, boolean editable) {
        this.surveyBo = surveyBo;
        this.editable = editable;
        sf.getHistory(sf.getUserOfSurvey(surveyBo));
        for (SurveySetBo surveySet : sf.getHistory(sf.getUserOfSurvey(surveyBo))) {
        	if (surveySet.getSurveyBoSet().get(SurveyKind.TeamMember) == surveyBo) {
        		surveyKind = SurveyKind.TeamMember;
        	}
        	if (surveySet.getSurveyBoSet().get(SurveyKind.TeamManager) == surveyBo) {
        		surveyKind = SurveyKind.TeamManager;
        	}
        	if (surveySet.getSurveyBoSet().get(SurveyKind.Consensus) == surveyBo) {
        		surveyKind = SurveyKind.Consensus;
        	}
        }
        
    }

    @Override
    public void run() {
        String question;
        ArrayList<String> options;
        for (RatingBo rating : surveyBo.getRatingBoList()) {
            options = new ArrayList<>();
            CompetenceBo competence = rating.getCompetenceBo();

            //CRAFT COMPETENCE
            if (competence instanceof CraftCompetenceBo) {
                System.out.println(competence.getName());
                //IF LEVELS
                if (!((CraftCompetenceBo) competence).getPossibilityMap().isEmpty()) {
                    for (Entry<Integer, String> entry : ((CraftCompetenceBo) competence).getPossibilityMap().entrySet()) {
                        options.add(entry.getValue());
                    }
                }
                //NO LEVELS
                else {
                    options.add("no experience");
                    options.add("little experience");
                    options.add("enough experience, I can work autonomously");
                    options.add("broad expertise, considered expert across teams/organisation");
                }
                question = ((CraftCompetenceBo) competence).getDescription();
            } else {
                //BEHAVIORAL COMPETENCE
                if (competence instanceof BehavioralCompetenceBo) {
                    System.out.println(competence.getName());
                    for (Entry<Integer, String> entry : ((BehavioralCompetenceBo) competence).getPossibilityMap().entrySet()) {
                        //MINIMUM LEVEL
                        if (entry.getKey() == ((BehavioralCompetenceBo) competence).getMinLevel()) {
                            options.add("---MINIMUM---" + entry.getValue() + "---MINIMUM---");
                        }
                        //NORMAL LEVEL
                        else {
                            options.add(entry.getValue());
                        }
                    }
                    question = ((BehavioralCompetenceBo) competence).getDescription();
                }
                //
                else {
                    //OPERATING UNIT COMPETENCE - DOMAIN COMPETENCE
                    options.add("");
                    options.add("");
                    options.add("");
                    options.add("");
                    question = competence.getName();
                }
            }
            rating = handleRating(rating, question, options);
            rf.updateRating(rating);
        }
    }

    private RatingBo handleRating(RatingBo rating, String question, List<String> options){
        if (editable) {
            rating.setLevel(Prompter.promptRating(question, options));
            if (surveyKind != SurveyKind.TeamManager) {
                rating.setEnergy(Prompter.yesOrNoQuestion("Does it give you energy?"));
            }
            if (surveyKind == SurveyKind.Consensus) {
                if (Prompter.yesOrNoQuestion("Do you want to enter a comment?")) {
                    ((ConsensusRatingBo) rating).setComment(Prompter.promptString("Please enter the comment:"));
                }
            }
        } else {
            System.err.println("CHOSEN: " + rating.getLevel());
            if (surveyKind != SurveyKind.TeamManager) {
                System.err.println("ENERGY: " + rating.isEnergy());
            }
            if (surveyKind == SurveyKind.Consensus) {
                if (!((ConsensusRatingBo) rating).getComment().isEmpty()) {
                    System.err.println("COMMENT: " + ((ConsensusRatingBo) rating).getComment());
                }
            }
        }
        return rating;
    }
}
