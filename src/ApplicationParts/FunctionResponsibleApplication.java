package ApplicationParts;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import ApplicationHelpers.Prompter;
import ApplicationHelpers.Searcher;
import colruyt.pcrsejb.bo.function.FunctionBo;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.bo.userPrivilege.FunctionResponsibleUserPrivilegeBo;
import colruyt.pcrsejb.bo.userPrivilege.PrivilegeTypeBo;
import colruyt.pcrsejb.facade.FunctionFacade;
import colruyt.pcrsejb.facade.UserFacade;

public class FunctionResponsibleApplication implements ApplicationPart {

    UserFacade uf = new UserFacade();
    FunctionFacade functionFacade = new FunctionFacade();

    @Override
    public void run() {
        boolean back = false;
        List<String> menuOptions = new ArrayList<>();
        menuOptions.add("Add Function Responsible");
        menuOptions.add("Remove Function Responsible");
        menuOptions.add("Go Back");

        while (!back){
            String choice = Prompter.promptMenu("Please choose an option:", menuOptions);
            switch (choice){
                case "Add Function Responsible":
                    addFunctionResponsible();
                    break;
                case "Remove Function Responsible":
                    removeFunctionResponsible();
                    break;
                case "Go Back":
                    back = true;
                    break;
                default:
                    break;
            }
        }
    }

    private void removeFunctionResponsible() {
        UserBo userBo = Prompter.promptMenu("Which function responsible do you want to delete?",
                uf.getAllFunctionResponsibles());

    }

    private void addFunctionResponsible() {
        UserBo userBo = Searcher.askUserUsingShortString();
        FunctionBo functionBo = Prompter.promptMenu("Please give the function", functionFacade.getAllFunctionNames());

        List<String> countries = Arrays.asList(new String[]{"BE", "FR", "IN"});
        String country = Prompter.promptMenu("Please give the country.", countries);

        FunctionResponsibleUserPrivilegeBo frp = new FunctionResponsibleUserPrivilegeBo(PrivilegeTypeBo.FUNCTIONRESPONSIBLE, true, functionBo, country);
        uf.addPrivilegeForUser(frp, userBo);
    }



}
