package ApplicationParts;

import java.util.ArrayList;
import java.util.List;

import ApplicationHelpers.Prompter;
import colruyt.pcrsejb.bo.enrolment.EnrolmentBo;
import colruyt.pcrsejb.bo.team.TeamBo;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.facade.TeamFacade;

public class TeamSurveyApplication implements ApplicationPart {

	private TeamFacade tf = new TeamFacade();
	
	
	@Override
	public void run() {
		TeamBo team = Prompter.promptMenu("Which team do you want to select for filling in a survey?", tf.getAllTeams());
		List<UserBo> users = new ArrayList<>();
		for (EnrolmentBo enrolment : team.getEnrolmentsBoHashSet()) {
			if (enrolment.isActive()) {
				users.add(enrolment.getUserBo());
			}
		}
		UserBo user = Prompter.promptMenu("Which user do you want to fill in the survey for?", users);
		ApplicationPart part = new SurveyApplication(user);
		part.run();
	}

}
