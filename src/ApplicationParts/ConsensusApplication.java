package ApplicationParts;

import java.util.ArrayList;
import java.util.List;

import ApplicationHelpers.Prompter;
import colruyt.pcrsejb.bo.survey.SurveySetBo;
import colruyt.pcrsejb.entity.survey.SurveyKind;
import colruyt.pcrsejb.facade.SurveyFacade;
import mainApp.AppStarter;

public class ConsensusApplication implements ApplicationPart {
	private SurveyFacade sf = new SurveyFacade();
	
	@Override
	public void run() {
        boolean back = false;
        List<String> menuOptions = new ArrayList<>();
        SurveySetBo surveySetBo = sf.getLastSurveySetForUser(AppStarter.currentUser);
		if (surveySetBo.getSurveyBoSet().get(SurveyKind.TeamMember).getDateCompleted() != null && surveySetBo.getSurveyBoSet().get(SurveyKind.TeamManager).getDateCompleted() != null) {
			menuOptions.add("Start consensus");
		}
		else {
			System.out.println("Consensus cannot be started yet. ");
		}
        menuOptions.add("Go back");
        while (!back) {
            String choice = Prompter.promptMenu("Please choose your functionality?", menuOptions);
            switch (choice) {
                case "Start consensus":
                	ApplicationPart part = new SurveyApplication(true);
        			part.run();
                    break;
                case "Go back":
                    back = true;
                default:
                    break;
            }
        }
	}

}
