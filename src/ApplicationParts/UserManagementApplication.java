package ApplicationParts;

import ApplicationHelpers.Prompter;
import ApplicationHelpers.Searcher;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.bo.userPrivilege.PrivilegeTypeBo;
import colruyt.pcrsejb.bo.userPrivilege.UserPrivilegeBo;
import colruyt.pcrsejb.facade.PrivilegeFacade;
import colruyt.pcrsejb.facade.UserFacade;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

/**
 * Klasse waar een admin een user kan toevoegen, verwijderen en/of privilege kan
 * toewijzen
 * 
 * @author jda1mbw
 *
 */
public class UserManagementApplication implements ApplicationPart {

	UserFacade uf = new UserFacade();
	PrivilegeFacade pf = new PrivilegeFacade();

	@Override
	public void run() {
		boolean back = false;
		List<String> menuOptions = new ArrayList<>();
		menuOptions.add("Add user");
		menuOptions.add("Remove user");
		menuOptions.add("Adapt user privileges");
		menuOptions.add("Go back");
		while (!back) {
			System.out.println("---------------");
			System.out.println("USER MANAGEMENT");
			System.out.println("---------------");
			String choice = Prompter.promptMenu("Please choose your functionality?", menuOptions);
			switch (choice) {
			case "Add user":
				addUser();
				break;
			case "Remove user":
				removeUser();
				break;
			case "Adapt user privileges":
				adaptUserPrivileges();
				break;
			case "Go back":
				back = true;
			default:
				break;
			}
		}
	}

	/**
	 * Methode voor het toevoegen van een user
	 */
	private void addUser() {
		System.out.println("-------------");
		System.out.println("USER CREATION");
		System.out.println("-------------");
		String email = Prompter.promptEmailAddress("Please give the email address.");
		String firstName = Prompter.promptAlphabeticString("Please give the first name.");
		String lastName = Prompter.promptAlphabeticString("Please give the last name.");
		String password = Prompter.promptAlphabeticString("Please give the password.");

		UserBo user = new UserBo();
		user.setEmail(email);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setPassword(password);
		System.out.println("user created...");
		user.setPrivilegeBoHashSet(askPrivileges());
		uf.saveUser(user);
	}

	/**
	 * Methode voor het verwijderen van een user
	 */
	private void removeUser() {
		System.out.println("-------------");
		System.out.println("USER DELETION");
		System.out.println("-------------");
		UserBo user = Searcher.askUserUsingShortString();
		uf.removeUser(user);
		System.out.println("user deleted...");
	}

	/**
	 * Methode voor het aanpassen van de userprivileges
	 */
	private void adaptUserPrivileges() {
		System.out.println("---------------");
		System.out.println("USER PRIVILEGES");
		System.out.println("---------------");
		UserBo user = Searcher.askUserUsingShortString();
		user.setPrivilegeBoHashSet(askPrivileges());
		uf.saveUser(user);
		System.out.println("Privileges set...");
	}

	/**
	 * Methode voor het toevoegen van privileges
	 */
	private HashSet<UserPrivilegeBo> askPrivileges() {
		HashSet<UserPrivilegeBo> privileges = new HashSet<>();
		boolean addPrivs = true;
		UserPrivilegeBo priv;
		PrivilegeTypeBo privType;
		while (addPrivs) {
			privType = Prompter.promptMenu("What property do you want to add? (select one at a time)", Arrays.asList(PrivilegeTypeBo.values()));
			priv = new UserPrivilegeBo(privType, true);
			privileges.add(priv);
			addPrivs = Prompter.yesOrNoQuestion("Do you want to add another property?");
		}
		return privileges;
	}

}
