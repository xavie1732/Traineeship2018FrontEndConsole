package ApplicationParts;

import java.util.ArrayList;
import java.util.List;

import ApplicationHelpers.Prompter;
import ApplicationHelpers.Searcher;
import colruyt.pcrsejb.bo.enrolment.EnrolmentBo;
import colruyt.pcrsejb.bo.team.TeamBo;
import colruyt.pcrsejb.bo.user.UserBo;
import colruyt.pcrsejb.facade.TeamFacade;

public class TeamManagementApplication implements ApplicationPart {
	TeamFacade teamFacade = new TeamFacade();
	TeamBo team = null;

	@Override
	public void run() {
		boolean back = false;
		changeTeam();
		List<String> menuOptions = new ArrayList<>();
		menuOptions.add("Change team");
		menuOptions.add("View team");
		menuOptions.add("Add user");
		menuOptions.add("Remove user");
		menuOptions.add("Go back");
		while (!back) {
			System.out.println("---------------");
			System.out.println("TEAM MANAGEMENT");
			System.out.println("---------------");
			System.out.println("Team: " + team.getName());
			String choice = Prompter.promptMenu("Please choose your functionality?", menuOptions);
			switch (choice) {
				case "Change team":
					changeTeam();
					break;
				case "Add user":
					AddUser();
					break;
				case "Remove user":
					RemoveUser();
					break;
				case "View team":
					ViewTeam();
					break;
				case "Go back":
					back = true;
				default:
					break;
			}
		}
	}
	
	private void changeTeam() {
		team = Prompter.promptMenu("Which team do you want to manage?", teamFacade.getAllTeams());
	}

	private void AddUser() {
		System.out.println("Which user would you like to add?");
		UserBo user = Searcher.askUserUsingShortString();
		teamFacade.addUserToTeam(team, user);
	}
	
	private void RemoveUser() {
		List<UserBo> users = new ArrayList<>();
		for (EnrolmentBo enrolment : team.getEnrolmentsBoHashSet()) {
			if (enrolment.isActive()) {
				users.add(enrolment.getUserBo());
			}
		}
		UserBo user = Prompter.promptMenu("Which user do you want to delete?", users);
		teamFacade.removeUserFromTeam(team, user);
	}
	
	private void ViewTeam() {
//		System.out.println("TEAM: " + team.getName());
//		for (EnrolmentBo enrolment : team.getEnrolmentsBoHashSet()) {
//			if (enrolment.isActive() && enrolment.getPrivilegeBo() instanceof TeamManagerPrivilege) {
//				System.out.println("MANAGER: " + enrolment.getUserBO().getFirstName() + " " + enrolment.getUserBO().getFirstName());
//			}
//		}
//		for (EnrolmentBo enrolment : team.getEnrolmentsBoHashSet()) {
//			if (enrolment.isActive() && (!(enrolment.getPrivilegeBo() instanceof TeamManagerPrivilege))) {
//				System.out.println("- " + enrolment.getUserBO().getFirstName() + " " + enrolment.getUserBO().getFirstName());
//			}
//		}
	}

	

	

}
