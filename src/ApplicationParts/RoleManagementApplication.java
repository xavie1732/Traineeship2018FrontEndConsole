package ApplicationParts;

import ApplicationHelpers.Prompter;
import colruyt.pcrsejb.bo.role.RoleBo;

import java.util.ArrayList;
import java.util.List;

public class RoleManagementApplication implements ApplicationPart{

    @Override
    public void run(){
        boolean back = false;
        List<String> menuOptions = new ArrayList<>();
        menuOptions.add("Add Role");
        menuOptions.add("Remove Role");
        menuOptions.add("Get All Roles");
        menuOptions.add("Go back");
        while (!back) {
            System.out.println("---------------");
            System.out.println("ROLE MANAGEMENT");
            System.out.println("---------------");
            String choice = Prompter.promptMenu("Please choose your functionality?", menuOptions);
            switch (choice) {
                case "Add Role":
                    AddRole();
                    break;
                case "Remove Role":
                    RemoveRole();
                    break;
                case "Get All Roles":
                    getAllRoles();
                    break;
                case "Go back":
                    back = true;
                default:
                    break;
            }
        }
    }
    
    /**
     * Methode voor het toevoegen van een rol
     */
    private void AddRole(){
    	System.out.println("-------------");
    	System.out.println("Craft Competence CREATION");
        System.out.println("-------------");
        String name = Prompter.promptString("Please give the name of the role.");

        RoleBo role = new RoleBo();
        role.setName(name);

        System.out.println("Role created...");

        //rolef.addRole(role);
    }
    /**
     * Methode voor het verwijderen van een rol 
  	*/
	private void RemoveRole(){
		System.out.println("-------------");
		System.out.println("Role DELETION by name");
        System.out.println("-------------");
        String name = Prompter.promptString("Please give the name of the role.");

        //RoleBo role = RoleBo
        //CraftCompetenceBo user = name;
        // TODO
        //craftCompetencef.removeCraftCompetenceByName(name);
        System.out.println("Role deleted...");
    }
    
    /**
     * Methode om alle rollen weer te geven
     */
    private void getAllRoles(){
        //TODO
    }
    
}

