package ApplicationParts;

import ApplicationHelpers.Prompter;
import colruyt.pcrsejb.bo.survey.SurveyBo;
import colruyt.pcrsejb.bo.survey.SurveySetBo;
import colruyt.pcrsejb.bo.userPrivilege.PrivilegeTypeBo;
import colruyt.pcrsejb.bo.userPrivilege.TeamMemberUserPrivilegeBo;
import colruyt.pcrsejb.bo.userPrivilege.UserPrivilegeBo;
import colruyt.pcrsejb.entity.survey.SurveyKind;
import mainApp.AppStarter;

import java.util.ArrayList;
import java.util.List;

public class ViewHistoryApplication implements ApplicationPart {
    @Override
    public void run() {

        //AppStarter.currentUser
        ArrayList<SurveySetBo> surveySetBos = new ArrayList<>();


        for (UserPrivilegeBo privBo : AppStarter.currentUser.getPrivilegeBoHashSet()) {
            if (privBo.getPrivilegeType() == PrivilegeTypeBo.TEAMMEMBER) {
                surveySetBos.addAll(((TeamMemberUserPrivilegeBo) privBo).getSurveySetBoTreeSet());
            }
        }



        SurveySetBo surveySetChoice = Prompter.promptMenu("Which survey set (year)?", surveySetBos);
        SurveyBo memberSurvey;
        SurveyBo managerSurvey;
        SurveyBo consensusSurvey;

        memberSurvey = surveySetChoice.getSurveyBoSet().get(SurveyKind.TeamMember);
        managerSurvey = surveySetChoice.getSurveyBoSet().get(SurveyKind.TeamManager);
        consensusSurvey = surveySetChoice.getSurveyBoSet().get(SurveyKind.Consensus);

        List<String> menuOptions = new ArrayList<>();
        menuOptions.add("Member survey");
        menuOptions.add("Manager survey");
        menuOptions.add("Consensus survey");

        String choice = Prompter.promptMenu("Which kind of survey?", menuOptions);
        SurveyBo surveyChoice = null;

        switch (choice) {
            case "Member survey":
                surveyChoice = memberSurvey;
                break;
            case "Manager survey":
                surveyChoice = managerSurvey;
                break;
            case "Consensus survey":
                surveyChoice = consensusSurvey;
                break;
            default:
                break;
        }
        ApplicationPart ap = new SurveyApplication(surveyChoice, false);
        ap.run();
    }


}
